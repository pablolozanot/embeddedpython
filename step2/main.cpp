#include <iostream>
#include <cstdio>
#include <cstdlib>     /* system, NULL, EXIT_FAILURE */
#include <boost/python.hpp>
#include <boost/python/suite/indexing/vector_indexing_suite.hpp>
#include <Python.h>
#include <string>
#include <vector>

#include <utilities.h>

namespace py = boost::python;

std::string parse_python_exception();

int main(int argc, char *argv[])
{
   
    Py_Initialize();

    py::class_<std::vector<double> >("PyVec")
		.def(boost::python::vector_indexing_suite<std::vector<double> >()); //To pass a std::vector


    py::object main_module = py::import("__main__");
    py::object main_namespace = main_module.attr("__dict__");


    try
    {
        std::vector<double> my_cpp_vector;
        my_cpp_vector.push_back(1.);
        my_cpp_vector.push_back(12.);
		//Define a short vector, let's see what we can do with it!
		
        py::object my_mod = py::import("python_calling");
		py::object my_fun = my_mod.attr("my_fun2");
        py::object my_result = my_fun(my_cpp_vector);
		
		//Pl
		int return_npvector_size = py::extract<double>(my_result[0]);
	for(unsigned int k=0; k<return_npvector_size; ++k)
	{
        std::cout << py::extract<double>(my_result[1][k]) << std::endl ;
	}
    }
    catch(boost::python::error_already_set const &)
    {
        std::string perror_str = PyBoostUtilities::parse_python_exception();
        std::cout << "Error in Python: " << perror_str << std::endl;
    }
 
    Py_Finalize();
    return 0;
}

