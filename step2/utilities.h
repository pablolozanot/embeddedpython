// http://thejosephturner.com/blog/post/embedding-python-in-c-applications-with-boostpython-part-2/
#include <iostream>
#include <boost/python.hpp>
#include <boost/python/suite/indexing/vector_indexing_suite.hpp>
#include <Python.h>

// Parses the value of the active python exception
// NOTE SHOULD NOT BE CALLED IF NO EXCEPTION
namespace PyBoostUtilities
{
	std::string parse_python_exception();	
}



