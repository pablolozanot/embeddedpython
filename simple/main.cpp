#include <iostream>
#include <cstdio>
#include <cstdlib>     /* system, NULL, EXIT_FAILURE */
#include <boost/python.hpp>
#include <boost/python/suite/indexing/vector_indexing_suite.hpp>
#include <Python.h>
#include <string>
#include <vector>

namespace py = boost::python;

std::string parse_python_exception();

int main(int argc, char *argv[])
{

    Py_Initialize(); //Initialize the Python interpreter; https://docs.python.org/2/c-api/init.html
	
    py::object main_module = py::import("__main__");
    py::object main_namespace = main_module.attr("__dict__");

    py::exec("print 'Hello world!'", main_namespace);
    
	Py_Finalize(); 
    return 0;
}

