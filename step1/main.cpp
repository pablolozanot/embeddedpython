#include <iostream>
#include <cstdio>
#include <cstdlib>     /* system, NULL, EXIT_FAILURE */
#include <boost/python.hpp>
#include <boost/python/suite/indexing/vector_indexing_suite.hpp>
#include <Python.h>
#include <string>
#include <vector>

#include <utilities.h>

namespace py = boost::python;

int main(int argc, char *argv[])
{
 
    Py_Initialize();

	py::object main_module = py::import("__main__");
    py::object main_namespace = main_module.attr("__dict__");

	//Catch errors to identify problems in the Python code
	// See http://thejosephturner.com/blog/post/embedding-python-in-c-applications-with-boostpython-part-2/
    try
    {
   
        py::object my_mod = py::import("python_calling");
		//python_calling.py must be somewhere in your PYTHONPATH!
		
        py::object my_fun = my_mod.attr("my_fun");
        py::object my_result = my_fun(2.);

		std::cout << py::extract<double>(my_result) << std::endl ;


		// Now a string
		py::object my_fun_str = my_mod.attr("my_fun3");
        py::object my_result_str = my_fun_str("Hello");
		char const* c_str = py::extract<char const*>(my_result_str);
        std::string myCppString = c_str;
		std::cout <<myCppString<<std::endl;
 
    }
    catch(boost::python::error_already_set const &)
    {
        std::string perror_str = PyBoostUtilities::parse_python_exception();
        std::cout << "Error in Python: " << perror_str << std::endl;
    }
 
    Py_Finalize();
    return 0;
}

