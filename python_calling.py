# -*- coding: utf-8 -*-

import numpy as np
from scipy import special, optimize
import matplotlib.pyplot as plt


def my_fun(a):

 ## You can use Python features however you like
    f = lambda x: -special.jv(3, x)
    sol = optimize.minimize(f, 1.0) # Example from SciPy documentation
    x = np.linspace(0, 10, 5000)
    plt.plot(x, special.jv(3, x), '-', sol.x, -sol.fun, 'o')
    plt.savefig('plot.png', dpi=96)

 ## You can take values from C++ and change them
    b=2*a
    return b
    
    
def my_fun2(a):
    b=np.array([])
    b=np.append(b,a[1])
    b=np.append(b,a[0])

    return np.size(b) , b
    
def my_fun3(str_a):
    if(str_a=="Hello"):
        print "Hello to you to"
    else:
        print "please say Hello"    
    str_a = "Modified in Pyhton"
    return str_a
